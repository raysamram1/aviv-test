from scipy.spatial import distance


def manhattan(a, b):
    """Calculate Manhatan distance between 2 points"""
    return sum(abs(val1 - val2) for val1, val2 in zip(a, b))


def calculate_distance(
    list_paths_1: list, list_paths_2: list, images_features: list
):
    """Calculate minimal distance between a pair of 2 list of images"""
    list_features_1 = [images_features[img] for img in list_paths_1]
    list_features_2 = [images_features[img] for img in list_paths_2]
    distances = distance.cdist(list_features_1, list_features_2)
    return distances.min(axis=(0, 1))
