import os

import matplotlib.pyplot as plt
import pandas as pd
import torch
import torchvision.transforms as TF
from PIL import Image
from scipy import spatial
from torchvision import models
from tqdm import tqdm

device = "cuda" if torch.cuda.is_available() else "cpu"
scaler = TF.Scale((224, 224))
normalize = TF.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
to_tensor = TF.ToTensor()

model = models.resnet152(pretrained=True).to(device)
model = torch.nn.Sequential(*(list(model.children())[:-1]))


def get_image_features(images: list) -> list:
    """
    Calculate image features (from ResNet) of each image.
    Input:
        - images : list of PIL images.
    Output:
        - features : list of features of each image.
    """
    features = list()
    for img in images:
        t_img = normalize(to_tensor(scaler(img))).unsqueeze(0).to(device)
        feat = model(t_img)
        features.append(feat.cpu().detach().numpy().flatten())
    return features


def get_single_image_features(img):
    """Get ResNet feature of a single image"""
    t_img = normalize(to_tensor(scaler(img))).unsqueeze(0).to(device)
    feat = model(t_img)
    return feat.cpu().detach().numpy().flatten()


def get_all_images_features(path):
    """Extract for each post its pictures' features"""
    images_features = dict()
    cnt = 0
    for subdir, dirs, files in os.walk(path):
        for file in files:
            filepath = subdir + os.sep + file

            if (
                filepath.endswith(".jpeg")
                or filepath.endswith(".jpg")
                or filepath.endswith(".png")
            ):
                images_features[file] = get_single_image_features(
                    Image.open(filepath).convert("RGB")
                )
            cnt += 1
    return images_features


def get_image_pixels(images):
    """Flatten representation of pixels of an image"""
    return [list(img.getdata()) for img in images]


def get_related_images(id, path_pictures):
    """Get image related to a listing_id"""
    return [
        filename
        for filename in os.listdir(path_pictures)
        if filename.startswith(str(id))
        and (
            filename.endswith("jpg")
            or filename.endswith("jpeg")
            or filename.endswith("png")
        )
    ]


def detect_images_duplicates(path_pictures, path_listing_csv):
    """
    Detect duplicates based on images of the posts.
    Input:
        - path_pictures : path to the directory containing the pictures.
        - path_listing_csv : path to the CSV file containing the posts
                    and their informations.
    Output:
        - df : Dataframe of duplicates.

    """
    listing_csv = pd.read_csv(path_listing_csv)
    images_features = get_all_images_features(path_pictures)
    flipped = {}

    for key, value in images_features.items():
        stringified = str(value.tolist())
        if stringified not in flipped:
            flipped[stringified] = [key]
        else:
            flipped[stringified].append(key)

    nbr_examples_show = 3
    listing_id_1 = list()
    listing_id_2 = list()

    for key, value in flipped.items():
        if len(flipped[key]) > 1:
            # print('\n\n\nDuplicate list : ', flipped[key])
            for el in flipped[key]:
                for el2 in flipped[key]:
                    listing_id_1.append(el.split("_")[0])
                    listing_id_2.append(el2.split("_")[0])
                if nbr_examples_show > 0:
                    plt.figure()
                    plt.title("Post id " + el)
                    plt.imshow(Image.open(os.path.join(path_pictures, el)))
                    plt.show()
            nbr_examples_show -= 1

    detected_duplicates = pd.DataFrame(
        {"listing_id_1": listing_id_1, "listing_id_2": listing_id_2}
    )
    detected_duplicates = detected_duplicates[
        detected_duplicates["listing_id_1"]
        != detected_duplicates["listing_id_2"]
    ]
    detected_duplicates = detected_duplicates.drop_duplicates()
    detected_duplicates = detected_duplicates.sort_values(
        by=["listing_id_1", "listing_id_2"], ascending=True
    )

    # BASED ON DISTANCE
    distances = dict()
    for img_name in tqdm(images_features.keys()):
        distances[img_name] = dict()
        for img_name_2 in images_features.keys():
            if img_name != img_name_2:
                distances[img_name][img_name_2] = spatial.distance.cosine(
                    images_features[img_name], images_features[img_name_2]
                )

    min_distance = 0.2
    listing_id_1 = list()
    listing_id_2 = list()

    for index, row in tqdm(listing_csv.iterrows(), total=listing_csv.shape[0]):
        images_paths = get_related_images(row["listing_id"], path_pictures)
        for img in images_paths:
            d = distances[img]
            closest_image = min(d, key=d.get)
            closest_post = closest_image.split("_")[0]
            closest_image_distance = d[closest_image]
            # print("cl : ", closest_image_distance)
            if closest_image_distance < min_distance:
                listing_id_1.append(row["listing_id"])
                listing_id_2.append(closest_post)
    distance_duplicate_df = pd.DataFrame(
        {"listing_id_1": listing_id_1, "listing_id_2": listing_id_2}
    )
    frames = [detected_duplicates, distance_duplicate_df]
    return pd.concat(frames)
