import os

import matplotlib.pyplot as plt
from PIL import Image
from tqdm import tqdm


def display(row, path_pictures):
    """
    Function to display all images of original + duplicate post.
    Input:
        - row : Dataframe row containing listing id
        of both duplicate and original.
        - path_pictures
    """
    images_of_announcement = [
        os.path.join(path_pictures, filename)
        for filename in os.listdir(path_pictures)
        if filename.startswith(str(row["listing_id_1"]))
    ]
    images_of_duplicate = [
        os.path.join(path_pictures, filename)
        for filename in os.listdir(path_pictures)
        if filename.startswith(str(row["listing_id_2"]))
    ]

    print("\n\n\nOriginal")
    for image in images_of_announcement:
        image1 = Image.open(image)
        plt.figure()
        plt.title("Original " + str(row["listing_id_1"]))
        plt.imshow(image1)
        plt.show()

    print("\nDuplicate")
    for image in images_of_duplicate:
        image1 = Image.open(image)
        plt.figure()
        plt.title("Duplicate " + str(row["listing_id_2"]))
        plt.imshow(image1)
        plt.show()


def calculate_accuracy(detected_csv, duplicate_csv):
    """
    Calculate accuracy between predictions and ground truth.
    Input:
        - detected_csv : CSV which was created through the prediction process.
        - duplicate_csv : ground truth duplicates.
    Output:
        - accuracy : score which ideally should be equal to 1.
    """
    positive = 0
    for index, row in tqdm(
        duplicate_csv.iterrows(), total=duplicate_csv.shape[0]
    ):
        # print('checking for row : \n', row)
        if (
            (detected_csv["listing_id_1"] == row["listing_id_1"])
            & (detected_csv["listing_id_2"] == row["listing_id_2"])
        ).any() or (
            (detected_csv["listing_id_1"] == row["listing_id_2"])
            & (detected_csv["listing_id_2"] == row["listing_id_1"])
        ).any():
            positive += 1
    return positive / duplicate_csv.shape[0]


def calculate_false_negatives(detected_csv, duplicate_csv, nbr_ex_display=3):
    fsp = 0
    for index, row in tqdm(
        detected_csv.iterrows(), total=detected_csv.shape[0]
    ):
        # print('checking for row : \n', row)
        if not (
            (
                (duplicate_csv["listing_id_1"] == row["listing_id_1"])
                & (duplicate_csv["listing_id_2"] == row["listing_id_2"])
            ).any()
            or (
                (duplicate_csv["listing_id_1"] == row["listing_id_2"])
                & (duplicate_csv["listing_id_2"] == row["listing_id_1"])
            ).any()
        ):
            if nbr_ex_display > 0:
                print(row)
                display(row)
            fsp += 1
            nbr_ex_display -= 1
    return fsp


def show_not_detected(detected_csv, duplicate_csv, nbr_ex_display=3):
    fsp = 0
    for index, row in tqdm(
        duplicate_csv.iterrows(), total=duplicate_csv.shape[0]
    ):
        # print('checking for row : \n', row)
        if not (
            (
                (detected_csv["listing_id_1"] == row["listing_id_1"])
                & (detected_csv["listing_id_2"] == row["listing_id_2"])
            ).any()
            or (
                (detected_csv["listing_id_1"] == row["listing_id_2"])
                & (detected_csv["listing_id_2"] == row["listing_id_1"])
            ).any()
        ):
            if nbr_ex_display > 0:
                print(row)
                display(row)
            else:
                return fsp
            fsp += 1
            nbr_ex_display -= 1
    print("Not detected ", fsp)
    return fsp
