import nltk
import pandas as pd
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from scipy.spatial import distance
from transformers import CamembertModel, CamembertTokenizer

tokenizer = CamembertTokenizer.from_pretrained("camembert-base")
camembert = CamembertModel.from_pretrained("camembert-base")
camembert.eval()
nltk.download("punkt")


def extract_embedding(row):
    """
    Create an embedding column based on textual column.
    Input:
        - row : Row of a dataframe containing a string.
    Output:
        - encoded : Encoded string by camemBERT.
    """
    tokenized_sentence = " ".join(
        [x for x in word_tokenize(row) if x not in stopwords.words("french")]
    )
    return tokenizer.encode(
        tokenized_sentence,
        padding="max_length",
        truncation=True,
        max_length=512,
        add_special_tokens=True,
    )


def detect_description_duplicates(path_listing_csv: str):
    """
    Detect duplicates based on description of the posts.
    Input:
        - path_listing_csv : path to the CSV file containing
            the posts and their informations.
    Output:
        - df : Dataframe of duplicates.

    """
    listing_csv = pd.read_csv(path_listing_csv)
    listing_csv["embedding"] = listing_csv["description"].apply(
        extract_embedding
    )
    embeddings = listing_csv["embedding"].to_list()
    listing_ids = listing_csv["listing_id"].to_list()

    listing_id_1 = list()
    listing_id_2 = list()
    for idx, emb in enumerate(embeddings):
        for idx2, emb_2 in enumerate(embeddings):
            if idx == idx2:
                continue
            if distance.cosine(emb, emb_2) < 0.2:
                listing_id_1.append(listing_ids[idx])
                listing_id_2.append(listing_ids[idx2])

    return pd.DataFrame(
        {"listing_id_1": listing_id_1, "listing_id_2": listing_id_2}
    )
