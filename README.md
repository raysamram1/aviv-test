# AVIV group technical test
This is a repository to store the result of the technical test for aviv group Machine Learning Engineer.

# Goal
Automatical detection of duplicates in announcement's posts.

# Data 
Data was provided by AVIV and is stored in the ```data``` folder.
```
- listings.csv : CSV file that contains all the listings to use. Each listing has an unique ID, features such as Area, room_count, floor_count etc. and a description. 

- duplicate_listings.csv : CSV file that contains pairs of duplicates listings. Each listing within the pairs is referenced by its ID

- pictures : Directory containing all pictures for each listing. Name of files are composed like this : <Listing_ID>_<picture_number>.<extension>
```

# How to run ?
Install the libraries :
```
pip install -r requirements.txt
```
Run the project :
```
python main.py 
```
The arguments are the following :
```
- path_listing_csv : Path to CSV of listing.
- path_pictures : Path to the directory of pictures.
- path_output_csv : Path to output for duplicate CSV.
```
