import argparse

import pandas as pd

from src.cv import detect_images_duplicates
from src.nlp import detect_description_duplicates


def main(args):
    df_nlp_duplicates = pd.DataFrame({"listing_id_1": [], "listing_id_2": []})
    df_cv_duplicates = pd.DataFrame({"listing_id_1": [], "listing_id_2": []})
    print("Detecting description duplicates ...")
    df_nlp_duplicates = detect_description_duplicates(args.path_listing_csv)
    print("Detecting images duplicates ...")
    df_cv_duplicates = detect_images_duplicates(
        args.path_pictures, args.path_listing_csv
    )
    frames = [df_nlp_duplicates, df_cv_duplicates]
    detected_duplicates = pd.concat(frames)
    detected_duplicates.to_csv(args.path_output_csv)
    print(f"Export to {args.path_output_csv} finished !")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--path_listing_csv", help="Path to CSV of listing.", type=str
    )
    parser.add_argument(
        "--path_pictures", help="Path to the directory of pictures.", type=str
    )
    parser.add_argument(
        "--path_output_csv", help="Path to output for duplicate CSV.", type=str
    )
    args = parser.parse_args()
    main(args)
